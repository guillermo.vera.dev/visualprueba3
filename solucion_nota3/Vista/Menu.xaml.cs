﻿using LibreriaDeClases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Vista
{
    /// <summary>
    /// Lógica de interacción para Menu.xaml
    /// </summary>
    public partial class Menu : Window
    {
        public List<Producto> productos { get; set; }
        public Menu menu { get; set; }
        List<Producto> po1  { get;  }
    public Menu()
        {
            po1= new List<Producto>();
            InitializeComponent();
        }

        private void btn_Buscar_Click(object sender, RoutedEventArgs e)
        {
            List<Producto> p = productos.Where(x => x.Descripcion == txt_buscar.Text).ToList();

            if (p.Count == 0)
            {
                MessageBox.Show("Producto no encontrado");
            }
            else {
                MessageBoxResult result = MessageBox.Show("Quiere agregar este Producto ?", "Agregar", MessageBoxButton.YesNoCancel);
                Producto produc = p.First();
                produc.Stock = 1;
                switch (result)
                                    {
                    case MessageBoxResult.Yes:
                        
                        break;
                    case MessageBoxResult.No:
                        MessageBox.Show("Oh well, too bad!", "My App");
                        break;
                    case MessageBoxResult.Cancel:
                        MessageBox.Show("Nevermind then...", "My App");
                        break;
                }
            }
        }
    }
}
