﻿using LibreriaDeClases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Vista
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public List<Cliente> clientes = new List<Cliente>();
        public List<Producto> productos = new List<Producto>();
             
        public void ClienteAdd(Cliente c) {
            clientes.Add(c);
        }

        public MainWindow()
        {
            InitializeComponent();
        }

        private void btn_producto_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            Producto p1 = new Producto
            {
                Descripcion = "Pizza",
                Precio = 1200,
                Stock = 100
            };
            Producto p2 = new Producto
            {
                Descripcion = "Papas Fritas",
                Precio = 1000,
                Stock = 5000
            };
            Producto p3 = new Producto
            {
                Descripcion = "Completo",
                Precio = 1100,
                Stock = 100
            };
            Producto p4 = new Producto
            {
                Descripcion = "Bebida 3L",
                Precio = 1500,
                Stock = 80
            };

            productos.Add(p1);
            productos.Add(p2);
            productos.Add(p3);
            productos.Add(p4);


        }
    }
}
