﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace LibreriaDeClases
{
    public class Direccion
    {
        private string _calle;

        public string Calle
        {
            get { return _calle; }
            set {
                string calle = value;
                if (calle.Length < 1)
                    throw new ArgumentOutOfRangeException("Calle no valido");
                else
                    _calle = calle;
            }
        }

        private string _descripcion;

        public string Descripcion
        {
            get { return _descripcion; }
            set {
                string descripcion = value;
                if (descripcion.Length < 1)
                    throw new ArgumentOutOfRangeException("Descripcion no valido");
                else
                    _descripcion = descripcion;
            }
        }

        private int _numero;

        public int Numero
        {
            get { return _numero; }
            set {
                int numero = value;
                if (numero < 0)
                    throw new ArgumentOutOfRangeException("Numero no valido");
                else
                    _numero = numero;
            }
        }

       public Comuna Comuna { get; set; }
        
        protected void Init() {
            Calle = "Los vilos";
            Descripcion = "Pa Los vios";
            Numero = 454;
            Comuna = Comuna.LasCondes;
        }

        public Direccion() {
            Init();
        }
    }
}
