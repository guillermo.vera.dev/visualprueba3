﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriaDeClases
{
    public class Persona
    {
        private string _nombre;
        public string Nombre
        {
            get { return _nombre; }
            set {
                string nombre = value;
                if (nombre.Length < 1)
                    throw new ArgumentOutOfRangeException("Nombre no valido");
                else
                    _nombre = nombre;
            }
        }

        private string _apellido;
        public string Apellido
        {
            get { return _apellido; }
            set {
                string apellido = value;
                if (apellido.Length < 1)
                    throw new ArgumentOutOfRangeException("Apellido no valido");
                else
                    _apellido = apellido;
            }
        }
        private int _telefono;

        public int Telefono
        {
            get { return _telefono; }
            set {
                int telefono = value;
                if (telefono < 9)
                    throw new ArgumentOutOfRangeException("Telefono no valido");
                else
                    _telefono = telefono;
            }
        }
        protected void Init() {
            Nombre = "javier";
            Apellido = "Plaza";
            Telefono = 911122233;
        }

        public Persona() {
            Init();
        }



    }
}
