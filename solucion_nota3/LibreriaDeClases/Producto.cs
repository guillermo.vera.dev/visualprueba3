﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriaDeClases
{
    public class Producto
    {
        public string Descripcion { get; set; }

        private int _precio;

        public int Precio
        {
            get { return _precio; }
            set {
                int precio = value;
                if (precio < 10)
                    throw new ArgumentOutOfRangeException("Precio no valido");
                else
                    _precio = precio;
            }
        }

        private int _stock;

        public int Stock
        {
            get { return _stock; }
            set {
                int stock = value;
                if (stock < 0)
                    throw new ArgumentOutOfRangeException("stok no valido");
                else
                    _stock = stock;
            }
        }

        protected void Init() {
            Descripcion = "Descripcion";
            Precio = 50;
            Stock = 0;
        }

        public Producto() {
            Init();
        }

    }
}
