﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriaDeClases
{
    public class Cliente:Persona
    {
        public List<Direccion> Direccion { get; set; }

        protected new void Init() {
            Nombre = "javier";
            Apellido = "Plaza";
            Telefono = 911122233;
            Direccion = new List<Direccion>();
        }

        public Cliente() {
            Init();
        }

    }
}
