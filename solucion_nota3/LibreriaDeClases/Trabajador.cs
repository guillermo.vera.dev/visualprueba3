﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriaDeClases
{
    public class Trabajador:Persona
    {

        private string _rut;
        public string Rut
        {
            get { return _rut; }
            set {
                string rut = value;
                if (rut.Length < 8)
                    throw new ArgumentOutOfRangeException("Rut no valido");
                else
                    _rut = rut;
            }
        }

        private string _cargo;

        public string Cargo
        {
            get { return _cargo; }
            set {
                string cargo = value;
                if (cargo.Length > 1)
                    throw new ArgumentOutOfRangeException("Cargo no valido");
                else
                    _cargo = cargo;
            }
        }
        public Direccion Direccion { get; set; }

        protected new void Init()
        {
            Nombre = "javier";
            Apellido = "Plaza";
            Telefono = 911122233;
            Direccion = new Direccion();
            Rut = "11222333-k";
            Cargo = "Empleado";
        }

        public Trabajador()
        {
            Init();
        }
    }
}
