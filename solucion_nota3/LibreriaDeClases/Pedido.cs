﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriaDeClases
{
    public class Pedido
    {
        public Direccion Direccion { get; set; }

        public Cliente Cliente { get; set; }
        public FormaDePago Pago { get; set; }

        public  Menu Menu{ get; set; }

        public Estado Estado { get; set; }

        protected void Init() {
            Direccion = new Direccion();
            Cliente = new Cliente();
            Pago = FormaDePago.Efectivo;
            Menu = new Menu();
            Estado = Estado.Ingresado;
        }

        public Pedido() {
            Init();
        }

    }
}
