﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriaDeClases
{
    public class Delivery
    {
        public int Id { get; set; }

        public Trabajador Trabajador { get; set; }

        private void Init()
        {
            Id = 0;
            Trabajador = new Trabajador();
        }

        public Delivery() {
            Init();
        }

    }
}
