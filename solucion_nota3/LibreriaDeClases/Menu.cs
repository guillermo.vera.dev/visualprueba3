﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriaDeClases
{
    public class Menu
    {
        public List<Producto> productos{ get; set; }

        private int _total;

        public int Total
        {
            get { return _total; }
            set { _total = value; }
        }

        public Menu() { }
    }
}
